export default class Process {
  constructor({id, name, description, created_at, admin, url_photo}) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.created_at = created_at;
    this.admin = admin;
    this.url_photo = url_photo;

  }

  getFormatedCreatedAt(){
    const dateHour = this.created_at.split(' ');

    const dates = dateHour[0].split('-');
    const hour = dateHour[1];

    return dates[2] + '/' + dates[1] + '/' + dates[0] + ' ' + hour;
  }

  getUrlPhoto(){
    if(this.url_photo.includes('.pdf')){
      return 'https://www.sodapdf.com/images/opdfs/choose_file.svg';
    }

    return this.url_photo;
  }
}
