import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {
    user: Object;

    constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,) {
        this.storage
            .get('user')
            .then(user => this.user = JSON.parse(user));

    }

    public formatDate(date){
        const dates = date.split('-');

        return dates[2] + '/' + dates[1] + '/' + dates[0];

    }

    public formatCpf(cpf){
        const first = cpf.slice(0, 3);
        const second = cpf.slice(3, 6);
        const third = cpf.slice(6, 9);
        const fourth = cpf.slice(9, 11);

        return first + '.' + second + '.' + third +'-'+fourth
    }

}
