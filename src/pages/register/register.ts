import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import {HomePage} from "../home/home";
import {LoginPage} from "../login/login";
import {Storage} from "@ionic/storage";
import {HTTP} from "@ionic-native/http";


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  name: string;
  username: string;
  email: string;
  password: string;
  passwordConfirmation: string;
  birthDate: string;
  cpf: string;

  constructor(
    public navCtrl: NavController,
    private http: HTTP,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: Storage
  ) {

  }

  async goToLogin() {
    await this.navCtrl.setRoot(LoginPage);
  }

  async sendForm() {
    let form = {
      name: this.name,
      username: this.username,
      email: this.email,
      password: this.password,
      password_confirmation: this.passwordConfirmation,
      birth_date: this.birthDate,
      cpf: this.cpf,
    };

    const loader = this.loadingCtrl.create();

    await loader.present();

    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    this.http.post('https://advsys.herokuapp.com/api/register', form, headers)
      .then((item: Object) => {
        loader.dismissAll();

        // @ts-ignore
        const response = JSON.parse(item.data);
        this.storage.set('user', JSON.stringify(response.data));
        this.storage.set('token', response.token);

        this.navCtrl.setRoot(HomePage);
      })
      .catch(error => {
        loader.dismissAll();

        this.showAlertError(JSON.parse(error.error).message);
      });
  }

  async showAlertError(error) {
    const alert = this.alertCtrl.create({
      title: 'Erro!',
      subTitle: error,
      buttons: ['OK']
    });

    await alert.present();
  }
}
