import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import {Storage} from "@ionic/storage";
import {HTTP} from "@ionic-native/http";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string;
  password: string;

  constructor(
    public navCtrl: NavController,
    private http: HTTP,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: Storage
  ) {
  }

  async goToRegister() {
    await this.navCtrl.setRoot(RegisterPage);
  }

  async sendForm() {
    let form = {
      email: this.email,
      password: this.password,
    };

    const loader = this.loadingCtrl.create();

    await loader.present();

    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    this.http.post('https://advsys.herokuapp.com/api/login', form, headers)
      .then((item) => {
        loader.dismissAll();
        // @ts-ignore
        const response = JSON.parse(item.data);

        this.storage.set('user', JSON.stringify(response.data));
        this.storage.set('token', response.token);

        this.navCtrl.setRoot(HomePage);
      })
      .catch(error => {
        loader.dismissAll();

        this.showAlertError(JSON.parse(error.error).message);
      });
  }

  async showAlertError(error) {
    const alert = this.alertCtrl.create({
      title: 'Erro!',
      subTitle: error,
      buttons: ['OK']
    });

    await alert.present();
  }

}
