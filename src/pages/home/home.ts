import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {ProcessPage} from "../process/process";
import {HTTP} from "@ionic-native/http";
import Process from "../../models/Process"
import {LoginPage} from "../login/login";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    user: Object;

    headers: Object;

    processes = [];

    constructor(
        public navCtrl: NavController,
        private storage: Storage,
        private http: HTTP,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
    ) {
        this.storage
            .get('user')
            .then(user => this.user = JSON.parse(user));

        this.loadProcesses();
    }

    async loadToken(){
        const token = await this.storage
            .get('token');

        this.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token

        };
    }

    async loadProcesses() {
        const loader = this.loadingCtrl.create();

        await loader.present();

        await this.loadToken();

        this.http.get('https://advsys.herokuapp.com/api/process?sort=created_at&order=desc', {}, this.headers)
            .then((item) => {
                loader.dismissAll();


                const response = JSON.parse(item.data);

                if(item.headers['new_token']){
                    this.storage.set('token', response.token);
                }

                this.processes = [];

                for(let process of response.data){
                    this.processes.push(new Process(process))
                }
            })
            .catch(error => {
                loader.dismissAll();

                this.showAlertError(JSON.parse(error.error).message);

                if(error.status == 401){
                  this.storage.remove('user');
                  this.storage.remove('token');
                  this.navCtrl.setRoot(LoginPage);
                }
            });
    }

    async refresh(refresher){
        await this.loadProcesses();

        refresher.complete();
    }

    async openProcess(process) {
        await this.navCtrl.push(ProcessPage, {process})
    }

    async showAlertError(error) {
        const alert = this.alertCtrl.create({
            title: 'Erro!',
            subTitle: error,
            buttons: ['OK']
        });

        await alert.present();
    }
}
