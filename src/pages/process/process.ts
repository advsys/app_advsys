import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import Process from '../../models/Process';

/**
 * Generated class for the ProcessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-process',
  templateUrl: 'process.html',
})
export class ProcessPage {
  public process: Process;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.process = this.navParams.get('process');
  }

  openImage(){
    window.open(this.process.url_photo, '_system');
  }
}
