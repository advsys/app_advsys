import {Component} from '@angular/core';
import {App, NavController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {LoginPage} from '../pages/login/login';
import {Storage} from "@ionic/storage";
import {HomePage} from "../pages/home/home";
import {ProfilePage} from "../pages/profile/profile";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any;

    public appPages = [
        {
            title: 'Processos',
            component: HomePage,
            icon: 'briefcase'
        },
        {
            title: 'Perfil',
            component: ProfilePage,
            icon: 'person'
        },
    ];

    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public storage: Storage) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();

            storage.get('user').then(user => {
                this.rootPage = user == null ? LoginPage : HomePage;
                splashScreen.hide();
            });

        });
    }

    public pushToPage(component){
        this.rootPage = component;
    }

    public async logout() {
        await this.storage.remove('user');
        await this.storage.remove('token');
        this.rootPage = LoginPage;
    }
}

